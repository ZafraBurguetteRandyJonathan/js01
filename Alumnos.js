//funcion objeto alumno
function alumno(_numcontrol,_nombre,_pa,_ma,_g){
    return {
    num_control:_numcontrol,
    nombre:_nombre,
    apellidopa:_pa,
    apellidoma:_ma,
    genero:_g,
    grupo:[]
    }
    }

    //creamos alumnos
alumno1=alumno("091452","Jorge","Perez","Lopez","M"); 
alumno2=alumno("096236","Alberto","Lopez","Perez","M"); 
alumno3=almuno("091613","Juan","Hernandez","Suarez","M");
alumno4=alumno("091414","Luis","Santiago","Lopez","M");
alumno5=alumno("091415","Alfaro","Antonio","Leyva","M");
alumno6=alumno("091416","Javier","Solis","Guzman","M");
alumno7=alumno("091417","Memelo","Cruz","Samaritano","M");
alumno8=alumno("091418","Samaritana","Cruz","Lopez","F");
alumno9=alumno("091419","Guadalupe","Sanchez","Mejia","F");
alumno10=alumno("091420","Evelyn","Sanchez","Diaz","F");
alumno11=alumno("091421","Alex","Diaz","Sanchez","M");
alumno12=alumno("091422","Gerardo","Gomez","Ramirez","M");
alumno13=alumno("091423","German","Lopez","Mateos","M");
alumno14=alumno("091424","Gabriel","Ortiz","Cruz","M");
alumno15=alumno("091425","Rubiel","Cruz","Mirangos","M");
alumno16=alumno("091426","Ramon","Alonso","Lopez","M");
alumno17=alumno("091427","Andre","Vela","Baños","F");
alumno18=alumno("091428","Ricardo","Baños","Solis","M");
alumno19=alumno("091429","Antoni","Hernandez","Blas","M");
alumno20=alumno("091430","Claribel","Benitez","Quetcha","F");
alumno21=alumno("091431","Carlos","Lopez","Perez","M");
alumno22=alumno("091432","Ducle","Mendes","Sanchez","F");
alumno23=alumno("091433","Arturo","Mendez","Ruiz","M");
alumno24=alumno("091434","Anahi","Mendieta","Lopez","F");
alumno25=alumno("091435","Carlos","Lopez","Mnedieta","M");
alumno26=alumno("091436","Javier","Lopez","Mendieta","M");
alumno27=alumno("091437","Homero","Simpson","Cruz","M");
alumno28=alumno("091438","Bartolomeo","Simpson","Mendez","M");
alumno29=alumno("091439","Goku","Perez","Lopez","M");
alumno30=alumno("091440","Marisol","Cruz","Lopez","M");

//compruebo que existan los objetos alumno
console.log(alumno7)
//agregamos un grupo a alumnos
alumno1.grupo=grupo1;
alumno2.grupo=grupo1;
alumno3.grupo=grupo1;
alumno4.grupo=grupo1;
alumno5.grupo=grupo1;
alumno6.grupo=grupo1;
alumno7.grupo=grupo1;
alumno8.grupo=grupo1;
alumno9.grupo=grupo1;
alumno10.grupo=grupo1;
//compruebo que el alumno tenga grupo
console.log(alumno1["grupo"]);

//crear grupo
function grupo(_clave,_nombre){
    return{
    clave:_clave,
    nombre:_nombre,
    alumnos:[],
    materias:[],
    profesores:[],
    }
    }

//creamos un objeto de grupo
grupo1=grupo("isa","A");
//asignamos alumnos a grupo
grupo1.alumnos[0]=alumno1;
grupo1.alumnos[1]=alumno2;
grupo1.alumnos[2]=alumno3
grupo1.alumnos[3]=alumno4
grupo1.alumnos[4]=alumno5
grupo1.alumnos[5]=alumno6
grupo1.alumnos[6]=alumno7
grupo1.alumnos[7]=alumno8
grupo1.alumnos[8]=alumno9

//comprobamos que estén los alumnos en el grupo
console.log(grupo1.alumnos[1]);

//agregamos materias al grupo
grupo1.materias[0]=materia1;
grupo1.materias[1]=materia2;
grupo1.materias[2]=materia3;
grupo1.materias[3]=materia4;
grupo1.materias[4]=materia5;

//comprobamos que estén las materias
console.log(grupo1["materias"][1].nombre)

//agregamos profesores al grupo
grupo1.profesores[0]=profesor1;
grupo1.profesores[1]=profesor2;
grupo1.profesores[2]=profesor3;
grupo1.profesores[3]=profesor4;
grupo1.profesores[4]=profesor5;

//comprobamos que esten los profesores
console.log(grupo1["profesores"][0].nombre)

//agreagamos horarios al grupo!!
grupo1.horarios[0]=horario1;
grupo1.horarios[1]=horario2;
grupo1.horarios[2]=horario3;
grupo1.horarios[3]=horario4;
grupo1.horarios[4]=horario5;

//comprobamos que esten los horarios en el grupo
console.log(grupo1["horarios"][3])


//crear horarios    
function horario(_clave){
        return{
        clave:_clave,
        materia:[],
        profesor:[]
        }
    }

//creamos los objetos de horario
horario1=horario("123");
horario2=horario("456");
horario3=horario("789");
horario4=horario("012");
horario5=horario("023");

//verificamos
console.log(horario5.profesor)

//asignamos materia y profesor a horarios
horario1.materia=materia1;
horario1.profesor=profesor1;
horario2.materia=materia2;
horario2.profesor=profesor2;
horario3.materia=materia3;
horario3.profesor=profesor3;
horario4.materia=materia4;
horario4.profesor=profesor4;
horario5.materia=materia5;
horario5.profesor=profesor5;


//crear materia
    function materia(_clave,_nombre){
        return{
        clave:_clave,
        nombre:_nombre
        }
    }

//creamos los objetos de materia    
    materia1=materia("1","Español");
    materia2=materia("2","Matematicas");
    materia3=materia("3","Historia");
    materia4=materia("4","Geografia");
    materia5=materia("5","Quimica");
    materia6=materia("6","Ciencias");
    materia7=materia("7","Fisica");

//verificamos que existan los objetos de materia
    console.log(materia1.nombre)
    
    Funcion objeto profesor   
function profesor(_clave,_nombre){
        return{
        clave:_clave,
        nombre:_nombre
        }
    }
    //creamos los objetos de profesor
     profesor1=profesor("1234","Rosario Perez Cruz");
     profesor2=profesor("5678","Tomy Cruz Ortiz");
     profesor3=profesor("90123","Demetrio Ortiz Jimenez");
     profesor4=profesor("45678","Rebeca Rios Silva");
     profesor5=profesor("9221","Angelica Petra Ramirez");
    //verificamos que existan los objetos de profesor
     console.log(profesor1)
     
     
     
     //insertar alumno

function insertar_alumno(num,nom){
    alumno(num,nom);
    num.grupo=grupo1;
    
}
insertar_alumno("8765","panfilo")

function insertar_profesor(clave,nom){
   profesor(clave,nom);
    clave.grupo=grupo1;
    
}

console.log(alumno.nombre)
     